    const basicDetailsBtn = document.querySelector("#basic-details-btn");
    const briefBioBtn = document.querySelector("#brief-bio-btn");
    const educationDetailsBtn = document.querySelector("#education-details-btn");

    const basicDetailsSection = document.querySelector("#basic-details");
    const briefBioSection = document.querySelector("#brief-bio");
    const educationDetailsSection = document.querySelector("#education-details");

    basicDetailsBtn.addEventListener("click", () => {
      basicDetailsSection.style.display = "block";
      briefBioSection.style.display = "none";
      educationDetailsSection.style.display = "none";
    });

    briefBioBtn.addEventListener("click", () => {
      basicDetailsSection.style.display = "none";
      briefBioSection.style.display = "block";
      educationDetailsSection.style.display = "none";
    });

    educationDetailsBtn.addEventListener("click", () => {
      basicDetailsSection.style.display = "none";
      briefBioSection.style.display = "none";
      educationDetailsSection.style.display = "block";
    });